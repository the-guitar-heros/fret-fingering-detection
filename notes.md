## Ensuring the model is trained properly
* Prevent overfitting (read TensorFlow's summary of strategies)
* Remove background
* More data
* Ensure pixels are exactly between [0, 1]: not more, not compressed twice
    * Both training and testing data
* Ensure dimensions are checked by printing or checking tf.shape()
